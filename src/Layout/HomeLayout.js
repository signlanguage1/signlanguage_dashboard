import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Badge, Dropdown } from 'antd';
import { MenuOutlined, UserOutlined, DownOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './HomeLayout.css';
import { Images, Colors, } from 'src/Theme';
import hashHistory from 'src/utils/HashHistory';
import { removeUserInformation } from 'src/utils/localStorage';

const { Header, Sider, Content } = Layout;

let menuList = [
  {
    key: '/waitCheck',
    img: Images.notice,
    title: '待審核',
    isShow: true
  },
  {
    key: '/check',
    img: Images.article,
    title: '審核紀錄',
    isShow: true
  },
];

const styles = {
  infoStyle: {
    width: '150px',
    display: 'flex',

    alignItems: 'center'
  },
  iconStyle: {
    width: '20px',
    height: '20px',
    cursor: 'pointer',
    marginRight: '20px'
  }
};

class HomeLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      currentKey: '',
    };
  }
  static propTypes = {
    children: PropTypes.object,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  handleMenuClick = (item) => {
    this.setState({ currentKey: item.key });
    hashHistory.push(item.key);
  }

  render() {
    const { children, user } = this.props;

    return (
      <Layout style={{ width: '100vw', height: '100vh' }}>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed} width={250} style={{ overflowY: 'auto' }}>
          <div className="logo"></div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} style={{ marginTop: '65px' }}>
            {menuList.map((item) => {
              if (item.isShow) {
                return (
                  <Menu.Item key={item.key} onClick={() => this.handleMenuClick(item)} style={{ display: 'flex', alignItems: 'center' }}>
                    <img src={item.img} style={{ width: '20px', marginRight: '30px' }} />{item.title}
                  </Menu.Item>
                )
              }
            })}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background-top" style={{ padding: 0, boxShadow: '0px 2px 10px #859DB1' }}>
            {React.createElement(MenuOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
            <div style={styles.infoStyle}>
              <img src={Images.logout} style={styles.iconStyle} />登出
            </div>
          </Header>
          <Content
            className="site-layout-background-content"
            style={{
              padding: '30px 20px',
              minHeight: 280,
              overflowY: 'scroll'
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
  }),
  (dispatch) =>
    bindActionCreators(
      {
      },
      dispatch,
    ),
)(HomeLayout);
