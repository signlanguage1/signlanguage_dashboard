import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Table, Modal, Input, Select, Tabs, Form, Radio, Row, Col } from 'antd';
import { Images } from 'src/Theme';

import "./UserUpdate.css"
import _ from 'lodash';
import moment from 'moment';
import { UserActions } from '../../Stores';
import { city, area } from '../../utils/location';
import HashHistory from '../../utils/HashHistory';

const { Option } = Select;
const { TabPane } = Tabs;

const styles = {
  root: {
    flexGrow: 1,
    // height: '100%',
    // overflowY: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  contentTop: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: '30px',
  },
  contentBottom: {
    width: '100%',
    height: '100%',
    marginTop: '20px',
    backgroundColor: '#fff',
    boxShadow: '0px 5px 20px rgba(176,195,211,0.16)',
    borderRadius: '4px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize: '16px',
    overflowY: 'auto'
  },
  inputStyle: {
    width: '470px',
    height: '40px',
    borderRadius: '5px',
  },
  formTop: {
    padding: '20px 25px 30px 25px',
    borderBottom: '#A6C1D3 1px solid'
  },
  formBottom: {
    width: '100%',
    display: 'flex',
    padding: '25px'
  },
  btnBlock: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  btnStyle: {
    width: '100px',
    height: '40px',
    color: '#fff',
    backgroundColor: '#004C7C',
    borderRadius: '4px',
    marginBottom: '40px'
  },
  // tabStyle:{
  //   minHeight:
  // }

};

class UserUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.updateForm = React.createRef();
    this.state = {
      isOpen: true,
      isLoading: false,
      viewModalVisible: false,
      userId: '',
      cityData: '',
      areaList: [],
      areaData: '',
    };
  }

  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {
    const { history, getUserInfo } = this.props;

    this.setState({
      isLoading: true
    });

    let temp = ""
    temp = history.location.pathname.split('/user/update/userId=');

    const callback = (value) => {
      this.setState({
        isLoading: false,
        userId: temp[1],
        areaData: this.props.user.area
      });
      this.handleAreaList(value.country)
    }
    getUserInfo(temp[1], callback);
  }

  componentWillUnmount() {
    const { resetUserInfo } = this.props;
    resetUserInfo();
  }

  handleUpdate = (value) => {
    const { updateUser } = this.props;
    const { userId } = this.state;

    this.setState({
      isLoading: true,
    });
    const callback = () => {
      this.setState({
        isLoading: false,
      });
      if (!_.isEmpty(value.new_password)) {
        this.handleChangePassword(value.new_password);
      }
      HashHistory.push('/user')
    }

    let payload = [];
    payload.push({
      ...value,
      user_id: userId
    })

    updateUser(userId, payload[0], callback);
  }

  handleChangePassword(value) {
    const { changePassword } = this.props;
    const { userId } = this.state;
    this.setState({
      isLoading: true
    })
    const callback = () => {
      this.setState({
        isLoading: false
      })
    }

    let payload = {};
    payload = { user_id: userId, new_password: value };
    changePassword(payload, callback);
  }

  //修改會員資料-表單變動
  handleFormChange = (change, all) => {
    if (_.has(change, 'country')) {
      this.updateForm.current.setFieldsValue({ area: undefined });
    }
  }

  //修改會員資料-地區選單option
  handleAreaList = (e) => {
    let areaListData = area.filter((item) => {
      return item.key == e;
    });

    this.setState({
      areaList: areaListData[0].list
    })
  }

  //訂購紀錄
  columns = [
    {
      width: '15%',
      title: '訂單編號',
      dataIndex: 'cart_order_id',
      key: 'cart_order_id',
      align: 'center',
    },
    {
      width: '15%',
      title: '訂單類型',
      dataIndex: 'is_inquiry',
      key: 'is_inquiry',
      align: 'center',
      // render: (value) => { value ? "願望清單" : "線上購物" }
    },
    {
      width: '15%',
      title: '狀態',
      dataIndex: 'cart_status_name',
      key: 'cart_status_name',
      align: 'center',

    },
    {
      width: '10%',
      title: '總計',
      dataIndex: 'cart_total',
      key: 'cart_total',
      align: 'center',
      defaultSortOrder: 'descend',
      // sorter: (a, b) => a.cart_total.localeCompare(b.cart_total),
    },
    {
      width: '15%',
      title: '購買日期',
      dataIndex: 'checkout_time',
      key: 'checkout_time',
      align: 'center',
      defaultSortOrder: 'descend',
      // sorter: (a, b) => {
      //   let aTimeString = moment(a.checkout_time).format('YYYY/MM/DD HH:mm:ss');
      //   let bTimeString = moment(b.checkout_time).format('YYYY/MM/DD HH:mm:ss');
      //   let aTime = new Date(aTimeString).getTime();
      //   let bTime = new Date(bTimeString).getTime();
      //   return aTime - bTime
      // },
    },
    {
      width: '15%',
      title: '操作',
      dataIndex: '',
      key: '',
      align: 'center',
      render: (value, record) =>
        <div>
          <img
            src={Images.detail}
            onClick={() => this.renderViewModal()} />
          {this.state.viewModalVisible && this.renderDetailModal()}
        </div>
    },
  ];

  listData = [
    {
      key: 1,
      id: '20210721001',
      type: '線上購物',
      status: '處理中',
      sum: '$450',
      date: '2021/09/09 08:05:00',
    },
    {
      key: 2,
      id: '20210721002',
      type: '線上購物',
      status: '處理中',
      sum: '$950',
      date: '2021/09/10 04:35:00',
    }
  ]

  //訂單詳細開關
  renderViewModal = () => {
    this.setState({
      viewModalVisible: true,
    });
  }

  //訂單詳細Modal
  renderDetailModal() {
    const { viewModalVisible } = this.state;

    return (
      <Modal
        title="詳細購買紀錄"
        visible={viewModalVisible}
        width={520}
        height={520}
        onCancel={() => this.setState({ viewModalVisible: false })}
        footer={null}
      >
      </Modal>
    )
  }

  //修改會員資料
  renderUserData = () => {
    const { user } = this.props;
    const { areaList } = this.state;

    if (_.isEmpty(user)) {
      return null;
    }
    return (
      <div style={{ overflowY: 'auto' }}>
        <Form
          ref={this.updateForm}
          labelCol={{ style: { width: '100%', fontSize: '16px', fontWeight: 'bold' } }}
          labelAlign="left"
          initialValues={{
            ...user,
            birth: moment(user.birth).format('YYYY-MM-DD'),
          }}
          // style={{ overflowY: 'auto' }}
          onFinish={this.handleUpdate}
          onValuesChange={this.handleFormChange}
        >
          <div style={styles.formTop}>
            <span
              style={{
                fontSize: '16px',
                fontWeight: 'bold',
                color: '#7D9EB5',
                margin: '0px 0px 20px 5px'
              }}
            >
              帳號：{user.account}
            </span>
            {/*
            <Form.Item
              name="level"
              label="會員等級"
              rules={[{ required: true, message: '此欄位不可為空！' }]}
            >
              <Select
                allowClear
                showArrow
                showSearch
                placeholder="請選擇會員等級"
                style={styles.inputStyle}
              >
                <Option value="青銅級">青銅級</Option>
                <Option value="白銀級">白銀級</Option>
              </Select>
            </Form.Item>
            */}
          </div>
          <div style={styles.formBottom}>
            <div style={{ width: '50%' }}>
              <Form.Item
                name="name"
                label="姓名"
              >
                <Input
                  placeholder='請輸入姓名'
                  style={styles.inputStyle}

                />
              </Form.Item>
              <Form.Item
                name="gender"
                label="性別"
              >
                <Radio.Group>
                  <Radio value={0} style={{ color: '#7D9EB5' }}>女</Radio>
                  <Radio value={1} style={{ color: '#7D9EB5' }}>男</Radio>
                  <Radio value={2} style={{ color: '#7D9EB5' }}>其他</Radio>
                </Radio.Group>
              </Form.Item>

              <Form.Item
                name="phone"
                label="電話"
              // rules={[{
              //   max: 10,
              //   message: '格式錯誤，必須為10碼的數字！',
              //   min: 10,
              //   message: '格式錯誤，必須為10碼的數字！'
              // }]}
              >
                <Input
                  placeholder="請輸入電話"
                  maxLength={10}
                  style={styles.inputStyle}
                />
              </Form.Item>
              <Row style={{ width: '470px' }}>
                <Col lg={12}>
                  <Form.Item
                    name="country"
                    label="地址"
                    style={{ marginRight: '10px', }}
                  >
                    <Select
                      allowClear
                      showArrow
                      showSearch
                      placeholder="請選擇縣市"
                      style={{ height: '40px', borderRadius: '5px' }}
                      onChange={this.handleAreaList}
                    >
                      {city.map((item) => {
                        return (
                          <Option key={item} value={item}>{item}</Option>
                        )
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col lg={12}>
                  <Form.Item
                    name="area"
                    label=" "
                    colon={false}
                  >
                    <Select
                      allowClear
                      showArrow
                      showSearch
                      placeholder="請選擇地區"
                      style={{ height: '40px', borderRadius: '5px' }}
                    >
                      {areaList ?
                        areaList.map((item, index) => {
                          return (
                            <Option key={index} value={item}>{item}</Option>
                          )
                        }) : null
                      }
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item
                name="addr"
                style={{ marginTop: '5px' }}
                placeholder='如：建行路一段...'
              >
                <Input
                  placeholder='請輸入地址'
                  style={styles.inputStyle}
                />
              </Form.Item>
              <Form.Item
                name="email"
                label="信箱"
                rules={[
                  {
                    type: 'email',
                    message: '格式錯誤！',
                  },
                ]}
              >
                <Input
                  placeholder="請輸入信箱"
                  style={styles.inputStyle}
                />
              </Form.Item>
              <Form.Item
                name="birth"
                label="生日"
              >
                <Input
                  style={styles.inputStyle}
                  disabled
                />
              </Form.Item>
              <Form.Item
                name="likes"
                label="喜好"
              // rules={[{ required: true, message: '此欄位不可為空！' }]}
              >
                <Select
                  allowClear
                  showArrow
                  showSearch
                  mode="multiple"
                  placeholder="請選擇喜好"
                  style={styles.inputStyle}
                  disabled
                >
                  <Option key="1" value="jack">Jack</Option>
                  <Option key="2" value="mac">Mac</Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="tags"
                label="店家註記喜好"
              // rules={[{ required: true, message: '此欄位不可為空！' }]}
              >
                <Select
                  allowClear
                  showArrow
                  showSearch
                  mode="multiple"
                  placeholder="請選擇店家註記喜好"
                  style={styles.inputStyle}
                  disabled
                >
                  <Option key="1" value="jack">Jack</Option>
                  <Option key="2" value="mac">Mac</Option>
                </Select>
              </Form.Item>

            </div>
            <div style={{ width: '50%' }}>
              <Form.Item
                name="new_password"
                label="新密碼"
                rules={[
                  {
                    required: true,
                    message: '此欄位必填',
                  },
                ]}
              >
                <Input.Password
                  placeholder='請輸入新密碼'
                  style={styles.inputStyle}
                />
              </Form.Item>
              <Form.Item
                name="passwordCheck"
                label="確認新密碼"
                // dependencies={['new_password']}
                rules={[
                  {
                    required: true,
                    message: '此欄位必填',
                  },
                  ({ getFieldValue }) =>
                  ({
                    validator(_, value) {
                      if (!value || getFieldValue('new_password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('與新密碼不一致'));
                    },
                  }),
                ]}
              >
                <Input.Password
                  placeholder='請輸入確認密碼'
                  style={styles.inputStyle}

                />
              </Form.Item>
            </div>
          </div>
          <div style={styles.btnBlock}>
            <Button htmlType="submit" style={styles.btnStyle}>儲存</Button>
          </div>
        </Form>
      </div >
    )
  }

  render() {

    return (
      <div style={{ width: '100%', height: '100%' }}>
        <div style={styles.root}>
          <div style={styles.wrapper}>
            <div style={styles.contentTop}>
              會員管理
            </div>
            <div style={styles.contentBottom}>
              <Tabs defaultActiveKey="1" >
                <TabPane tab="會員資料" key="1" style={styles.tabStyle}>
                  {this.renderUserData()}
                </TabPane>
                <TabPane tab="訂購紀錄" key="2" style={styles.tabStyle}>
                  <Table
                    id='table'
                    pagination={{
                      position: ['bottomCenter'],
                    }}
                    columns={this.columns}
                    dataSource={this.listData}
                    style={{ width: '100%' }}
                  />
                </TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    class: state.class,
    user: state.user.userInfo,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getUserInfo: UserActions.getUserInfo,
        updateUser: UserActions.updateUser,
        changePassword: UserActions.changePassword,
        resetUserInfo: UserActions.resetUserInfo,
      },
      dispatch,
    ),
)(UserUpdate);