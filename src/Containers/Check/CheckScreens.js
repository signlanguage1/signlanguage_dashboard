import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Table, Modal, Form, Input, Upload, message, Switch } from 'antd';
import { Images, Colors } from 'src/Theme';
import { LoadingOutlined, PlusOutlined, CheckOutlined } from '@ant-design/icons';
import { SortButton } from 'src/Components';
import "./CheckScreens.css"
import _ from 'lodash';
import Swal from 'sweetalert2';
import "src/Assets/Video/1.MOV"

const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  contentTop: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: '30px',
  },
  inputStyle: {
    border: '1px solid #7D9EB5',
    borderRadius: '5px',
    height: '40px',
    width: '100%',
    paddingLeft: '8px',
    color: '#6A67CE'
  },
  btnStyle: {
    backgroundColor: '#004C7C',
    width: '103px',
    height: '40px',
    color: 'white',
    borderRadius: '4px',
  },
  contentBottom: {
    width: '100%',
    height: '100%',
    marginTop: '20px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize: '20px',
    fontWeight: 'bold',
    backgroundColor: 'white',
  },
  contentBottomTitle: {
    marginLeft: '5px',
    marginBottom: '10px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'

  },
  imgWebDiv: {
    width: '180px',
    height: '100px',
    display: 'block',
    overflow: 'hidden',
    margin: 'auto'
  },
  imgPhoneDiv: {
    width: '80px',
    height: '105px',
    display: 'block',
    overflow: 'hidden',

    margin: 'auto'
  },
  imgStyle: {
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
    border: 'none',
  }
};

class CheckScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isLoading: false,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };


  componentDidMount = () => {
    const { getCarouselList } = this.props

    const callback = (value) => {
      console.log(value)
      this.setState({
        isLoading: false,
        total: value.length
      })
    }
  }


  render() {
    const { viewModalVisible, total, viewUpdateModalVisible } = this.state;


    // 分頁樣式
    const renderPagination = (current, type, originalElement) => {
      if (type === 'prev') {
        return <a className="table-prev" >&lt;&nbsp;&nbsp;  Prev</a>;
      } if (type === 'next') {
        return <a>Next  &nbsp;&nbsp;&gt;</a>;
      }
      return originalElement;
    }

    const columns = [
      {
        width: '10%',
        title: '審核日期',
        dataIndex: 'checkDate',
        key: 'checkDate',
        align: 'center',
        render: (value, record, index) => (
          value
        ),
      },
      {
        width: '10%',
        title: '辨識日期',
        dataIndex: 'date',
        key: 'date',
        align: 'center',
        render: (value, record) => (
          value
        ),
      },
      ,
      {
        width: '19%',
        title: '影片',
        dataIndex: 'video',
        key: 'video',
        align: 'center',
        render: (value, record) => (
          <div style={styles.imgWebDiv}>

            <video src={value} controls style={styles.imgStyle}></video>
            {/*
            <iframe src="https://drive.google.com/file/d/1TZ3ALjHgxme_yPUsiCuV_VZnScfDBpep/view?usp=sharing" style={styles.imgStyle} scrolling="no" frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
            <img src={record.banner_web_img}  />*/}
          </div>
        ),
      },
      {
        width: '14%',
        title: '辨識結果',
        dataIndex: 'output',
        key: 'output',
        align: 'center',
        render: (value, record) => (
          value
        )
      },
      {
        width: '20%',
        title: '審核結果',
        dataIndex: 'checkResult',
        key: 'checkResult',
        align: 'center',
        render: (value, record) => {
          return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              {value == 1 ?
                <div style={{ width: '80px', display: 'flex', justifyContent: 'center' }}>
                  <img src={Images.check} style={styles.imgStyle} />
                </div> : <div>
                  {value}
                </div>
              }
            </div>
          )
        }
      }
    ];

    let checkData = [
      {
        checkDate: "2021/09/24",
        date: "2021/09/24",
        video: "D:/two_year_technical_second/signLanguage/code/signLanguage_dashboard/src/Assets/Video/1.MOV",
        output: "謝謝",
        checkResult: "1"
      },

    ]

    return (
      <div style={{ width: '100%', height: '100%' }}>
        <div style={styles.root}>
          <div style={styles.wrapper}>
            <div style={styles.contentTop}>
              <span style={{ marginRight: '40px' }}>審核紀錄</span>
            </div>
            <div style={styles.contentBottom} >
              <Table
                columns={columns}
                dataSource={checkData}
                pagination={{
                  pageSize: 5,
                  showSizeChanger: false,
                  position: ['bottomCenter'],
                  itemRender: (current, type, originalElement) => renderPagination(current, type, originalElement)
                }}
                style={{ width: '100%', height: '90%', overflowY: 'auto' }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({

  }),
  (dispatch) =>
    bindActionCreators(
      {

      },
      dispatch,
    ),
)(CheckScreen);
