import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Form, Input, Row, Col } from 'antd';
import { Images, Colors } from 'src/Theme';
import { UserActions } from 'src/Stores';

import { LoginMiddleView } from 'src/Components';

const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '30px',
  },

};

const layout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isLoading: false,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  handleLogin = (value) => {
    const { login } = this.props;
    const { history } = this.props;
    const callback = () => {
      this.setState({
        isLoading: false,
      });
    }
    this.setState({
      isLoading: true,
    }, () => {
      login(value, callback);
    });
  };

  render() {
    const { isLoading } = this.state;
    return (
      <div style={{ width: '100%', height: '100%' }}>
        <LoginMiddleView handleLogin={this.handleLogin} />
      </div>
    );
  }
}

export default connect(
  (state) => ({
    class: state.class,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setToken: UserActions.setToken,
        login: UserActions.login,
      },
      dispatch,
    ),
)(LoginScreen);
