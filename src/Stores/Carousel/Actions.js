import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    getCarouselList: ['callback'],
    getCarouselListSuccess: ['payload'],
    getCarouselDetail: ['id', 'callback'],
    getCarouselDetailSuccess: ['payload'],
    createCarousel: ['payload', 'callback'],
    updateCarousel: ['payload', 'callback'],
    deleteCarousel: ['id', 'callback'],
    resetCarouselDetail: [''],
    changeCarouselStatus: ['id', 'callback'],
    sortCarousel: ['id', 'change', 'callback']
});

export const CarouselTypes = Types;
export default Creators;