/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { CarouselTypes } from './Actions';
import { getUserInformation } from 'src/utils/localStorage';
import { StaticRouter } from 'react-router';
import moment from 'moment';

export const getCarouselListSuccess = (state, { payload }) => {
  let temp = [];
  payload.list.map((item, index) => {
    temp.push({
      ...item,
      key: item.banner_id,
    });
  });
  return {
    ...state,
    CarouselList: temp,
  };
};

export const getCarouselDetailSuccess = (state, { payload }) => {
  return {
    ...state,
    Carousel: payload,
  };
};

export const resetCarouselDetail = (state, { }) => {
  console.log("in reset")
  return {
    ...state,
    Carousel: {},
  }
};

export const reducer = createReducer(INITIAL_STATE, {
  // export const reducer = createReducer(INITIAL_STATE, {
  [CarouselTypes.GET_CAROUSEL_LIST_SUCCESS]: getCarouselListSuccess,
  [CarouselTypes.GET_CAROUSEL_DETAIL_SUCCESS]: getCarouselDetailSuccess,
  [CarouselTypes.RESET_CAROUSEL_DETAIL]: resetCarouselDetail,
})
