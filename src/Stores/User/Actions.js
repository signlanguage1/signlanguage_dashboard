import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    setToken: ['token'],
    getUserInfo: ['id', 'callback'],
    getUserInfoSuccess: ['payload'],
    getUserList: ['callback', 'paging', 'search', 'loginOrder', 'createOrder'],
    getUserListSuccess: ['payload', 'paging'],
    createUser: ['payload', 'callback', 'paging', 'errorCallback'],
    updateUser: ['id', 'payload', 'callback'],
    deleteUser: ['id', 'callback', 'paging'],
    changePassword: ['payload', 'callback'],
    resetUserInfo: [''],


    login: ['payload', 'callback'],
    setUser: ['payload'],
    // deleteUser: ['payload', 'paging'],



    setMobile: ['payload'],
    // getUserById: ['payload', 'callback'],
    // getUserByIdSuccess: ['payload'],
    // forgetPassword: ['payload', 'callback'],

    // checkUpdate: ['payload', 'callback'],
    // getWeatherList: ['callback'],
    // getWeatherListSuccess: ['payload'],
});

export const UserTypes = Types;
export default Creators;