/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { UserTypes } from './Actions';
import { getUserInformation } from 'src/utils/localStorage';
import { StaticRouter } from 'react-router';
import moment from 'moment';

const userInfoStr = getUserInformation();
export const setToken = (state, { token }) => ({
  ...state,
  Token: token,

},
  console.log("token", token)
);

export const setUser = (state, { payload }) => ({
  ...state,
  ...payload,
  user: payload,
});

export const getUserListSuccess = (state, { payload, paging }) => {
  let temp = [];
  payload.map((item) => {
    temp.push({
      ...item,
      key: item.user_id,
    })
  })

  return {
    ...state,
    userList: temp,
    paging: paging,
  }

  // let temp = [];
  // let tempForm = [];
  // let currentUser = {};
  // let userList = [];

  // userList = payload.filter((item) => item.user_id === state.user_id);

  // payload.map((item) => {
  //   if (item.user_id === state.user.user_id) {
  //     currentUser = item;//自己
  //   }

  //   if (item.user_id !== state.user.user_id) {
  //     userList.push(item);//自己以外
  //   }

  //   temp.push({
  //     ...item,
  //     key: item.user_id,
  //   });

  //   tempForm.push({
  //     id: item.user_id,
  //     name: item.name,
  //   });

  // })

  // return {
  //   ...state,
  //   list: temp,
  //   paging: paging,
  //   // formList: tempForm,
  //   user: {
  //     ...state.user,
  //     ...currentUser,
  //   },
  //   userList,
  // }
};

export const getUserInfoSuccess = (state, { payload }) => {
  return {
    ...state,
    ...payload,
    userInfo: payload,
  }
};

export const setMobile = (state, { payload }) => {
  return {
    ...state,
    isMobile: payload,
  }
};

// export const getUserById = (state, { payload }) => {
//   return {
//     ...state,
//     userInfo: {},
//   }
// };

// export const getUserByIdSuccess = (state, { payload }) => {
//   return {
//     ...state,
//     userInfo: payload,
//   }
// };

// export const getWeatherListSuccess = (state, { payload }) => {
//   let tData = [];
//   let wxData = [];
//   let result = [];
//   payload.weatherElement.map((item) => {
//     let date = moment(item.time[0].startTime).format('YYYYMMDD');
//     item.time.map((timeItem, index) => {
//       if (index === 0) {
//         if (item.elementName === 'T') {
//           tData.push(timeItem.elementValue[0].value);
//         } else {
//           wxData.push(timeItem.elementValue[0].value);
//         }
//       }
//       if (date !== moment(timeItem.startTime).format('YYYYMMDD')) {
//         if (item.elementName === 'T') {
//           if (tData.length <= 6) {
//             tData.push(timeItem.elementValue[0].value);
//           }
//         } else {
//           if (wxData.length <= 6) {
//             wxData.push(timeItem.elementValue[0].value);
//           }
//         }
//       }
//       date = moment(timeItem.startTime).format('YYYYMMDD');
//     });
//   });

//   tData.map((item, index) => {
//     result.push({
//       t: item,
//       ws: wxData[index],
//     });
//   });

//   return {
//     ...state,
//     weatherData: {
//       ...payload,
//       result,
//     },
//   }
// };

export const resetUserInfo = (state, { }) => {
  return {
    ...state,
    userInfo: {},
  }
};

export const reducer = createReducer(userInfoStr ? JSON.parse(userInfoStr) : INITIAL_STATE, {
  [UserTypes.SET_TOKEN]: setToken,
  [UserTypes.SET_USER]: setUser,
  [UserTypes.GET_USER_LIST_SUCCESS]: getUserListSuccess,
  [UserTypes.GET_USER_INFO_SUCCESS]: getUserInfoSuccess,
  [UserTypes.RESET_USER_INFO]: resetUserInfo,

  [UserTypes.SET_MOBILE]: setMobile,
  // [UserTypes.GET_USER_BY_ID]: getUserById,
  // [UserTypes.GET_USER_BY_ID_SUCCESS]: getUserByIdSuccess,
  // [UserTypes.GET_WEATHER_LIST_SUCCESS]: getWeatherListSuccess,
});
