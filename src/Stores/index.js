import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';

import { reducer as UserReducer } from './User/Reducers';

export { default as UserActions } from './User/Actions';

export default () => {
  const rootReducer = combineReducers({
    user: UserReducer,
  });
  return configureStore(rootReducer, rootSaga);
};