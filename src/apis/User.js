// const prefix = 'api/client';

export default {
    login: () => '/api/auth',
    user: (id) => `/api/user?user_id=${id}`,
    getUserList: () => '/api/user/getAll',
    getAllPaging: (now_page, page_size, search, loginOrder, createOrder) => `/api/user/getAll?search=${search}&user_type_id&login_time=${loginOrder}&create_time=${createOrder}&now_page=${now_page}&page_size=${page_size}`,
    createUser: () => '/api/user/register',
    updateUser: () => '/api/user',
    deleteUser: () => '/api/user',
    changePassword: () => '/api/user/changePassword',

    // insertUser: () => '/api/User/Register',
};