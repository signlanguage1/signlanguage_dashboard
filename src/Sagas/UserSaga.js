import { put, call, select } from 'redux-saga/effects';
import { saveUserInformation } from 'src/utils/localStorage';

import {
    UserActions,
} from 'src/Stores';
import { Handler, User } from 'src/apis';
import { showMessage } from 'src/utils/message';
import _ from 'lodash';

export function* login({ payload, callback }) {
    try {
        console.log(payload)
        const { data: res } = yield call(
            Handler.post({ data: payload }),
            User.login(),
        );
        if (res.status === 200) {
            const setData = {
                Token: res.data.token,
                ...res.data.userData,
            }

            saveUserInformation(setData);
            yield put(UserActions.setUser(setData));
            // window.location.reload();
        }
    } catch (err) {
        console.log('err', err);
    } finally {
        if (callback) { callback() }
    }
}

export function* getUserInfo({ id, callback }) {
    try {
        const token = yield select((state) => state.user.Token);
        const { data: res } = yield call(
            Handler.get({ Authorization: token }),
            User.user(id),
        );

        if (res.status === 200) {
            const setData = {
                Token: token,
                ...res.data,
            }
            saveUserInformation(setData);
            yield put(UserActions.getUserInfoSuccess(res.data));
            if (callback) { callback(res.data) }
        }
    } catch (err) {
        console.log('err', err);
    }
}

export function* getUserList({ callback, paging = null, search = '', loginOrder = '', createOrder = '' }) {
    try {
        const token = yield select((state) => state.user.Token);

        let res;
        if (paging === null) {
            res = yield call(
                Handler.get({ Authorization: token }),
                User.getUserList(),
            );
        } else {
            res = yield call(
                Handler.get({ Authorization: token }),
                User.getAllPaging(paging.now_page, paging.page_size, search, loginOrder, createOrder),
            );

        }

        if (res.data.success) {
            yield put(UserActions.getUserListSuccess(res.data.data.list, res.data.paging));
            if (callback) { callback(res.data) }
        }
    } catch (err) {
        console.log('err', err);
    }
}

export function* createUser({ payload, callback, paging, errorCallback }) {
    try {
        const token = yield select((state) => state.user.Token);
        const { data: res } = yield call(
            Handler.post({ data: payload, Authorization: token }),
            User.createUser(),
        );
        if (res.success) {
            showMessage({ content: '新增成功' });
            yield put(UserActions.getUserList(() => { }, paging));
            if (callback) { callback() }
        }
    } catch (err) {
        console.log('err', err);
        if (errorCallback) { errorCallback() }
    }
}

export function* updateUser({ id, payload, callback }) {
    try {
        const token = yield select((state) => state.user.Token);

        const { data: res } = yield call(
            Handler.put({ data: payload, Authorization: token }),
            User.updateUser(),
        );
        if (res.status) {
            showMessage({ content: '修改成功' });
            if (callback) { callback() }
            yield put(UserActions.getUserInfo(id, callback));
        }
    } catch (err) {
        console.log('err', err);
    }
}

export function* deleteUser({ id, callback, paging }) {
    try {
        const token = yield select((state) => state.user.Token);
        const { data: res } = yield call(
            Handler.delete({ data: id, Authorization: token }),
            User.deleteUser(),
        );
        if (res.success) {
            showMessage({ content: '刪除成功' });
            if (callback) { callback() }
            yield put(UserActions.getUserList(() => { }, paging));
        }
    } catch (err) {
        console.log('err', err);
    }
}

export function* changePassword({ payload, callback, errorCallback }) {
    try {
        const token = yield select((state) => state.user.Token);
        const { data: res } = yield call(
            Handler.put({ data: payload, Authorization: token }),
            User.changePassword(),
        );
        if (res.status === 200) {
            showMessage({ content: '修改成功' });
            yield put(UserActions.getUserInfo(payload.user_id));
            if (callback) { callback() }
        }
    } catch (err) {
        console.log('err', err);
        if (errorCallback) { errorCallback() }
    }
}






// export function* forgetPassword({ payload, callback }) {
//     try {
//         const { data: res } = yield call(
//             Handler.get({}),
//             User.forget(payload),
//         );
//         if (res.success) {
//             showMessage({ content: '申請成功，前往信箱收信' });
//             if (callback) { callback() }
//         }
//     } catch (err) {
//         console.log('err', err);
//     }
// }



// export function* checkUpdate({ payload, callback }) {
//     try {
//         const token = yield select((state) => state.user.Token);
//         const { data: res } = yield call(
//             Handler.get({ Authorization: token }),
//             User.checkUpdate(payload),
//         );
//         if (res.success) {
//             if (callback) { callback(res.data.check_update) }
//         }
//     } catch (err) {
//         console.log('err', err);
//         if (callback) { callback(false) }
//     }
// }