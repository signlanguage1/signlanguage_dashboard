import { takeLatest, all } from 'redux-saga/effects';

import * as UserSaga from './UserSaga';

import { UserTypes } from 'src/Stores/User/Actions';

export default function* root() {
  yield all([
    takeLatest(UserTypes.LOGIN, UserSaga.login),
    takeLatest(UserTypes.GET_USER_INFO, UserSaga.getUserInfo),
    takeLatest(UserTypes.GET_USER_LIST, UserSaga.getUserList),
    takeLatest(UserTypes.CREATE_USER, UserSaga.createUser),
    takeLatest(UserTypes.UPDATE_USER, UserSaga.updateUser),
    takeLatest(UserTypes.DELETE_USER, UserSaga.deleteUser),
    takeLatest(UserTypes.CHANGE_PASSWORD, UserSaga.changePassword),
  ]);
};