import React, { Fragment } from 'react';
import { Switch, Redirect, Route, HashRouter } from 'react-router-dom';


import HomeLayout from 'src/Layout/HomeLayout';
import checkScreen from 'src/Containers/Check/CheckScreens';
import waitCheckScreen from 'src/Containers/WaitCheck/WaitCheckScreens';

class RouterPage extends React.Component {
  render() {
    return (
      <HomeLayout>
        <HashRouter>
          <Switch>
            <Route exact path="/check" component={checkScreen} />
            <Route exact path="/waitCheck" component={waitCheckScreen} />
            <Redirect from="/" to="/check" />
          </Switch>
        </HashRouter>
      </HomeLayout>
    )
  }
}

class LoginRoutes extends React.Component {
  render() {
    return (
      <Fragment>
        <RouterPage />
      </Fragment>
    );
  }
}

export default LoginRoutes;