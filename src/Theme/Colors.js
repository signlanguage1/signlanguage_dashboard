/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

const Colors = {
  dark: '#0c2563',
  primary: '#6A67CE',
  second: '#094884',
  third: '#089FE9',
  fourth: '#15307e',
  text: '#000000',
  linear1: '#094683',
  linear2: '#031425',
  search: '#123f6f',
  searchBlue: '#0168B7',
  focusText: '#F2AC3D',
  blackText: '#707070',
};

export default Colors;
