/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  // sider
  dashboard: require('src/Assets/Image/0_dashboard.svg'),
  carousel: require('src/Assets/Image/01_slider.svg'),
  notice: require('src/Assets/Image/02_notify.svg'),
  user: require('src/Assets/Image/03_user.svg'),
  article: require('src/Assets/Image/04_article.svg'),
  activity: require('src/Assets/Image/05_Event.svg'),
  type: require('src/Assets/Image/06_Classify.svg'),
  prop: require('src/Assets/Image/07_Attributes.svg'),
  product: require('src/Assets/Image/08_product.svg'),
  voucher: require('src/Assets/Image/09_discount.svg'),
  order: require('src/Assets/Image/10_order.svg'),
  orderStatus: require('src/Assets/Image/11_order_type.svg'),
  delivery: require('src/Assets/Image/12_Ship.svg'),
  store: require('src/Assets/Image/13_store.svg'),
  ad: require('src/Assets/Image/14_ad.svg'),
  qaC: require('src/Assets/Image/15_qa_c.svg'),
  qa: require('src/Assets/Image/16_qa.svg'),
  mail: require('src/Assets/Image/17_system_mail.svg'),
  form: require('src/Assets/Image/18_contact_form.svg'),

  // login
  login: require('src/Assets/Image/login.png'),
  loginBg: require('src/Assets/Image/loginbg.jpg'),


  //test
  pic1: require('src/Assets/Image/pic1.png'),
  pic2: require('src/Assets/Image/pic2.png'),

  //type
  open: require('src/Assets/Image/open-outline.png'),
  close: require('src/Assets/Image/close-outline.png'),

  //item
  editPen: require('src/Assets/Image/Icon-edit.png'),
  sort: require('src/Assets/Image/sort.png'),
  cancel: require('src/Assets/Image/cancel.png'),
  closeIcon: require('src/Assets/Image/icon-close.png'),
  openIcon: require('src/Assets/Image/Icon-open.png'),

  send: require('src/Assets/Image/send.png'),
  check: require('src/Assets/Image/check.png'),
  logout: require('src/Assets/Image/logout.png'),
};
